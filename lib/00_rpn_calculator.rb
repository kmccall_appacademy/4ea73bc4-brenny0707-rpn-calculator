class RPNCalculator

  def initialize
    @numbers = []
  end

  def push(num)
    @numbers << num
  end

  def plus
    perform_operation(:+)
  end

  def minus
    perform_operation(:-)
  end

  def divide
    perform_operation(:/)
  end

  def times
    perform_operation(:*)
  end

  def tokens(string)
    token_array = []
    string.split(" ").each do |char|
      token_array << char.to_i if char.to_i.to_s == char
      token_array << char.to_sym unless char.to_i.to_s == char
      end
      token_array
  end

  def evaluate(string)
    token_array = tokens(string)

    token_array.each do |token|
      case token
      when Integer
        push(token) #self.push(token)
      else
        perform_operation(token)
      end
    end
    value #self.value
  end

  def value
    p @numbers
    @numbers.last
  end

  private

  def perform_operation(op_symbol)

    raise "calculator is empty" if @numbers.size < 2
    stack2 = @numbers.pop
    stack1 = @numbers.pop

    case op_symbol
    when :+
      @numbers << stack1 + stack2
    when :-
      @numbers << stack1 - stack2
    when :*
      @numbers << stack1.to_f * stack2.to_f
    when :/
      @numbers << stack1.to_f / stack2.to_f
    else raise "such operation does not exist"
      @numbers << stack1
      @numbers << stack2
    end
  end

end
